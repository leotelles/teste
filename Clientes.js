const express = require('express');
const router = express.Router();
const dados = require('./Dados');

router.get('/clientes', async (req,res)=>{
    // res.send("entrou em /clientes")
    let cli = new dados.Cliente();
    res.render('home', { cliente : cli, lista : await dados.GetListaClientes()});
})

router.get('/clientes/:id',async (req,res)=>{
    let id = req.params.id; 
    let cli = dados.CarregaClientePorCodigo(id);
    res.render('home', { cliente : cli, lista : await dados.GetListaClientes()});
})

router.post('/clientes/del/:id', async (req,res) => {
    let id = req.params.id; 
    await dados.DeletaClientePorCodigo(id);
    res.redirect('/clientes');
})

router.post('/clientes', async (req,res) => {
    var erros = [];
    var erroMsg = '';
    let cli = new dados.Cliente();

    cli.nome = req.body.nome;
    cli.sobrenome = req.body.sobrenome;
    cli.senha = req.body.senha;
    cli.email = req.body.email;
    cli.sexo = req.body.sexo;
    cli.cpf = req.body.cpf;
    cli.rg = req.body.rg;
    cli.telefone = req.body.telefone;
    cli.estado_civil = req.body.estado_civil;
    cli.cep = req.body.cep;
    cli.endereco = req.body.endereco;


    console.log("faz as coias")
    console.log(req.body)

    if (cli.nome != '' && cli.sobrenome != '' && cli.senha != '' && cli.email != '' ){
        await dados.AddClientes(cli);
        cli = new dados.Cliente();
    }else{
        if (cli.nome == '' ){
            erros.push('Nome')
        }    
        if (cli.sobrenome == '' ){
            erros.push('Sobrenome')
        }
        if (cli.senha == '' ){
            erros.push('Senha')
        }
        if (cli.email == '' ){
            erros.push('Email')
        }
        if (erros.length > 0){
            erroMsg = 'Informe os campos (' + erros.join(',')+')';
        }
    }
    res.render('home', {cliente : cli, lista : await dados.GetListaClientes(), erroMsg : erroMsg});
});

module.exports = router;