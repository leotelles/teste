const fs = require('fs');
const util = require('util');
const NOME_ARQUIVO = 'lista.txt';

function Cliente(){
    this.codigo = '',
    this.nome = ''
    this.sobrenome = ''
    this.senha = ''
    this.email = ''
    this.sexo = ''
    this.cpf = ''
    this.rg = ''
    this.telefone = ''
    this.estado_civil = ''
    this.cep = ''
    this.endereco = ''


}
// var a = {codigo : 0, nome : ''};

var lista = [];

async function GetListaClientes(){
    await CarregarArquivo();
    return lista;
}

async function AddClientes(cliente){
    let existe = false;
    for (let i = 0; i < lista.length; i++) {
        if (lista[i].senha == cliente.senha && lista[i].nome == cliente.nome){
            lista[i] = cliente;
            existe = true;
            break;
        }        
    }
    if (!existe){
        cliente.codigo = lista.length+1;
        console.log("textoooooooooo");
        console.log(cliente);
        lista.push(cliente);
    }

    await SalvarArquivo();
}

async function SalvarArquivo(){
    let data = JSON.stringify(lista);
    // fs.writeFileSync(NOME_ARQUIVO,data);
    await util.promisify(fs.writeFile)(NOME_ARQUIVO,data)
}

async function CarregarArquivo(){

    try {
        let stat = await util.promisify(fs.stat)(NOME_ARQUIVO);
        if(stat.isFile){
            let data = await util.promisify(fs.readFile)(NOME_ARQUIVO);
    
            lista = JSON.parse(data.toString());
        }
    } catch (error) {}
    
}

function CarregaClientePorCodigo(id){
    for (let i = 0; i < lista.length; i++) {
        if ( lista[i].codigo == id){
            return lista[i];
        }        
    }
    return new Cliente();
}

async function DeletaClientePorCodigo(id){
    for (let i = 0; i < lista.length; i++) {
        if ( lista[i].codigo == id){
            lista.splice(i,1);
            await SalvarArquivo();
            break;
        }        
    }
}

module.exports.Cliente = Cliente;
module.exports.GetListaClientes = GetListaClientes;
module.exports.AddClientes = AddClientes;
module.exports.CarregaClientePorCodigo = CarregaClientePorCodigo;
module.exports.DeletaClientePorCodigo = DeletaClientePorCodigo;